import cv2
import numpy

def applyCanny(image):
    grayscale_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    blurred_image = cv2.GaussianBlur(grayscale_image, (5, 5), 0)
    return cv2.Canny(blurred_image, 50, 150)

def cropRegionOfInterest(image):
    height = image.shape[0]
    triangle = numpy.array([(200, height), (1100, height), (550, 250)])
    polygons = [triangle]
    mask = numpy.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)
    masked_image = cv2.bitwise_and(image, mask)
    return masked_image

def addLinesToImage(lines, image):
    line_image = numpy.zeros_like(image)
    if lines is not None:
        for line in lines:
            if line is not None:
                x1, y1, x2, y2 = line.reshape(4)
                cv2.line(line_image, (x1, y1), (x2, y2), [255, 0, 0], 10)
    return cv2.addWeighted(image, 0.8, line_image, 1, 1)

def calculateAveragedLines(image, lines):
    left_fit_bucket = []
    right_fit_bucket = []
    if (lines is not None):
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            parameters = numpy.polyfit((x1, x2), (y1, y2), 1)
            slope = parameters[0]
            intercept = parameters[1]
            if slope < 0:
                left_fit_bucket.append((slope, intercept))
            else:
                right_fit_bucket.append((slope, intercept))
        left_fit_average = calculateAverageFromArray(left_fit_bucket)
        right_fit_average = calculateAverageFromArray(right_fit_bucket)
        left_line = convertLineParametersToCoordinates(image, left_fit_average)
        right_line = convertLineParametersToCoordinates(image, right_fit_average)
        return numpy.array([left_line, right_line])

def calculateAverageFromArray(array):
    if len(array) == 0:
        return None
    return numpy.average(array, axis=0)

def convertLineParametersToCoordinates(image, line_parameters):
    if line_parameters is None:
        return None
    slope, intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1*(3/5)) 
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return numpy.array([x1, y1, x2, y2])

def findLanesInImage(image):
    canny_image = applyCanny(image)
    cropped_image = cropRegionOfInterest(canny_image)
    detected_lines = cv2.HoughLinesP(cropped_image, 2, numpy.pi/180, 100, numpy.array([]), minLineLength=40, maxLineGap=5)
    averaged_lines = calculateAveragedLines(image, detected_lines)
    line_image = addLinesToImage(averaged_lines, image)
    return line_image

# Logic for applying the lane finding logic on one test image
# image = cv2.imread('test_image.jpg')
# lane_image = numpy.copy(image)
# line_image = findLanesInImage(lane_image)
# cv2.imshow('lanes', line_image)
# cv2.waitKey(0)

# Logic for applying the lane finding logic on a test video
video_capture = cv2.VideoCapture('test2.mp4')
while(video_capture.isOpened()):
    frame_read_successful, frame = video_capture.read()
    if (frame_read_successful):
        line_image = findLanesInImage(frame)
        cv2.imshow('lanes', line_image)
        
    if cv2.waitKey(1) == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()